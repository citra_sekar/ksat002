
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String


def static "com.ksat002.function.util.DateUtil.today"() {
    (new com.ksat002.function.util.DateUtil()).today()
}

def static "com.ksat002.function.util.StringUtil.randomString"(
    	String chars	
     , 	int length	) {
    (new com.ksat002.function.util.StringUtil()).randomString(
        	chars
         , 	length)
}

def static "com.ksat002.function.util.StringUtil.getRandomEmail"(
    	int length	) {
    (new com.ksat002.function.util.StringUtil()).getRandomEmail(
        	length)
}

def static "com.ksat002.function.util.StringUtil.getRandomEmail"() {
    (new com.ksat002.function.util.StringUtil()).getRandomEmail()
}

def static "com.ksat002.function.util.StringUtil.getRandomPhoneNumber"(
    	int length	) {
    (new com.ksat002.function.util.StringUtil()).getRandomPhoneNumber(
        	length)
}

def static "com.ksat002.function.util.StringUtil.getRandomPhoneNumber"() {
    (new com.ksat002.function.util.StringUtil()).getRandomPhoneNumber()
}
