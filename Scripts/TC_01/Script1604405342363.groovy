import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.ksat002.function.util.StringUtil as SU

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.facebook.com/')

WebUI.click(findTestObject('Object Repository/Register Facebook/button_register'))

WebUI.setText(findTestObject('Object Repository/Register Facebook/input_firstName'), 'citra')

WebUI.setText(findTestObject('Object Repository/Register Facebook/input_lastName'), 'sekar')

//WebUI.setText(findTestObject('Object Repository/Register Facebook/input_noHP'), '081294804338')
String email = CustomKeywords.'com.ksat002.function.util.StringUtil.getRandomEmail'(15)

WebUI.setText(findTestObject('Object Repository/Register Facebook/Page_Facebook - Masuk atau Daftar/input_Kami Tidak Dapat Membuat Akun AndaKami tidak berhasil mendaftarkan Anda ke Facebook_reg_email__'), 
    email)

WebUI.setText(findTestObject('Object Repository/Register Facebook/Page_Facebook - Masuk atau Daftar/input_Kami Tidak Dapat Membuat Akun AndaKami tidak berhasil mendaftarkan Anda ke Facebook_reg_email_confirmation__'), 
    email)

WebUI.setEncryptedText(findTestObject('Object Repository/Register Facebook/input_Password'), '0KGWKd78bEnKGO/xbeuxdQ==')

WebUI.selectOptionByValue(findTestObject('Object Repository/Register Facebook/select_date'), '24', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Register Facebook/select_month'), '5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Register Facebook/select_year'), '1998', true)

WebUI.click(findTestObject('Object Repository/Register Facebook/input_sex'))

WebUI.click(findTestObject('Object Repository/Register Facebook/button_Daftar'))

WebUI.closeBrowser()

